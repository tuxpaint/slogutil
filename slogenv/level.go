package slogenv

import (
	"log/slog"
	"math"
	"os"
	"strconv"
	"strings"
)

func EnvLevel() slog.Level {
	return FromString(os.Getenv("GO_LOG"))
}

func FromString(x string) slog.Level {
	if val, err := strconv.Atoi(x); err == nil {
		return slog.Level(val)
	}
	switch strings.ToLower(x) {
	case "quiet":
		return math.MaxInt
	case "debug":
		return slog.LevelDebug
	case "info":
		return slog.LevelInfo
	case "warn":
		return slog.LevelWarn
	case "error", "err":
		return slog.LevelError
	}
	return slog.LevelInfo
}
